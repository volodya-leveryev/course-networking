# Диагностика работы HTTP средствами Chrome Web Tools и утилиты `curl`

## Chrome Web Tools

## WGET

`wget` — утилита командной строки для скачивания файлов `HTTP` / `HTTPS` / `FTP`
- задание имени файла `-O`
- скачивание по списку  `-i`
- продолжение загрузки `-С`
- изменение идентификатора клиента –user-agent=<string>

страница целиком:

```
wget ‐‐page-requisites ‐‐span-hosts ‐‐convert-links ‐‐adjust-extension http://example.com/dir/file
```

зеркалирование сайта:

```
wget ‐‐execute robots=off ‐‐recursive ‐‐no-parent ‐‐continue ‐‐no-clobber http://example.com/
```

## CURL

- Сохранение файлов `-o`, `-O`
- Продолжение загрузки `-С`
- Отображение заголовков `-I`
- Отправка данных POST `--data`

Литература:

- https://jsonplaceholder.typicode.com/
- https://flaviocopes.com/http-curl/
