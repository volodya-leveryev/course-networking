# Протокол ARP
Описание протокола было опубликовано в ноябре 1982 года в RFC 826. ARP был спроектирован для случая передачи IP-пакетов через сегмент Ethernet. При этом общий принцип, предложенный для ARP, может, и был использован и для сетей других типов.

Существуют следующие типы сообщений ARP: 
* запрос ARP (ARP request)
* ответ ARP (ARP reply). 

Время жизни записи в кэше оставлено на усмотрение разработчика. По умолчанию может составлять от десятков секунд (например, 20 секунд) до четырёх часов (Cisco IOS).

ARP-оповещение (ARP Announcement) — это пакет (обычно ARP запрос [3]), содержащий корректную SHA и SPA хоста-отправителя, с TPA, равной SPA. Это не разрешающий запрос, а запрос на обновление ARP-кэша других хостов, получающих пакет.

Распроненные сетевые атаки на ARP:
* ARP-спуфинг (ARP-spoofing, ARP-poisoning) — подделывание ARP-ответа с целью перехвата трафика, является разновидностью сетевой атаки типа MITM (англ. Man in the middle). Атака основана на недостатках протокола ARP.
* Переполнение CAM-таблиц - конечная цель - подслушивание трафика сети

Управляемые коммутаторы enterprise-уровня позволяют ограничить количество MAC-адресов на определенных портах (Cisco, HP, Dell и т.п.),

Управляемые коммутаторы имеют возможность программирования поведения каждого порта. Для них разрабатываются специализированные ОС (Cisco IOS, HP, Dell, )

Управляемые коммутаторы поддерживают протокол SNMP (Simple Network Management Protocol) — который позволяет управлять и диагностировать работу сетевых устройств.
