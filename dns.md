# Служба доменных имен (DNS, Domain Name Service)

Локальные файлы использовались до внедрения службы DNS и поддерживаются до сих пор для обратной совместимости

```
/etc/hosts
C:\Windows\System32\drivers\etc\hosts
```

```
192.168.1.1	example.com
```

На транспортном уровне DNS использует UDP/53 либо TCP/53.

Дерево доменных имен: все доменные образуют дерево. Корень этого дерева обозначается точкой. Полностью определенное доменное имя заканчивается на точку (то есть оно содержит перечень всех промежуточных доменных имен).

Валидные символы:
* `a-z`
* `0-9`
* `-`
* `.` — разделитель

Fully Qualified Domain Name (FQDN)

`www.s-vfu.ru`        ->      `www.s-vfu.ru.`

Fully Qualified Domain Name (FQDN) — полностью определенное доменное имя. FQDN заканчивается на точку.

Серверы DNS:
* основной ­— отвечает за какую-либо зону
* дополнительный — помогает основному, снижает нагрузку на его
* кеширующий — снижают нагрузку на DNS-серверы

Назначение DNS-сервера ответственным за зону называется делегированием.

Серверы (13 штук) которые отвечают за корень дерева доменных имен называются корневыми:
* A.ROOT-SERVERS.NET.
* B.ROOT-SERVERS.NET.
* ...
* M.ROOT-SERVERS.NET.

Работа этих серверов поддерживается крупными провайдерами и публичными организациями.

Их IP-адреса (IPv4 и IPv6) хранят все DNS-серверы в мире. 

Алгоритм работы нерекурсивного DNS-сервера:
* клиент делает запрос: `myserver.corp.example.com.`
* нерекурсивный DNS-сервер возвращает клиенту ответ о наилучшем известном ему DNS-сервере.

Алгоритм работы рекурсивного DNS-сервера:
* клиент делает запрос: `myserver.corp.example.com.`
* рекурсивный DNS-сервер сделает запрос корневым серверам о том, какой сервер управляет зоной `com.`
* рекурсивный DNS-сервер сделает запрос серверам зоны `com.` о том, какой сервер управляет зоной `example.com.`
* рекурсивный DNS-сервер сделает запрос серверам зоны `example.com.` о том, какой сервер управляет зоной `corp.example.com.`
* рекурсивный DNS-сервер сделает запрос серверам зоны `corp.example.com.` о том, какой IP-адрес у имени `myserver.corp.example.com.`
* рекурсивный DNS-сервер возвращает ответ клиенту

Высоконагруженные DNS-сервера — как правило являются нерекурсивными.

Большинство современных приложений автоматически определяют необходимое FQDN.
Если ваш хост находится в домене s-vfu.ru. то он при запросе www будет считать что это относительное имя и дополнит его своим доменом.

IPv4-адреса бывают:
* unicast — адрес 1 хоста
* multicast — адрес всех хостов в группе
* broadcast — адрес всех хостов в сети

IPv6-адреса бывают
* unicast
* multicast
* anycast — адрес любого хоста из группы

Корневые DNS-сервера имеют IPv6 anycast-адреса которые позволяют использовать ближайший из большого количества зеркальных DNS-серверов.

Top-Level Domain (TLD) — домен верхнего (первого) уровня:
* национальные (2-хбуквенные)
* .gov, .mil, .edu, .net, .com, .info, и т.д.

Request for Comments (RFC) — серия документов описывающих стандарты Internet. Важные документы RFC помечаются:
* Draft
* Best Current Practice (BCP)
* STD

For Your Information FYI — серия документов носящая информационный характер.

ICANN — некоммерческая организация управляющая работой Интернета
IANA — некоммерческая организация выдающая DNS-имена и IP-адреса
IETF — некоммерческая организация публикующая RFC

Части URL (Unique Resource Locator):
https://www.example.com/path/index.html
https://www.example.com/path/index.html?key1=value1&key2=value2
https://www.example.com/path/index.html#ancor
https://www.example.com:8080/path/index.html
https://user@www.example.com:8080/path/index.html
https://user:password@www.example.com:8080/path/index.html
ftp://
http://
mailto:vs.leverev@s-vfu.ru

бывают нестандартные схемы URL

URI — Unique Resource Identifier — уникальный идентификатор. Часто используется в XML.

Punycode — способ кодирования UTF-символов в разрешенных DNS символах. Благодаря Punycode можно создавать DNS-имена с использованием UTF-символов.

Типы записей DNS:
* SOA — Start of Authority — запись объявляющая зону
* NS — серверы DNS отвечающие за зону DNS
* MX — серверы обрабатывающие почту
* A — запись для IPv4-адреса
* AAAA — запись для IPv6-адреса
* CNAME — псевдоним доменного имени для другой записи
* SRV — явно указывают на наличие сервера (например, контроллер AD)
* PTR — обратный резолвинг (просмотр, разрешение)
* TXT — текстовый комментарий

Обратный резолвинг:
in-addr.arpa — зона для обратного резолвинга
23.5.168.192.in-addr.arpa. — запрос о доменном имени для IP-адреса 192.168.5.23

Расщепленный DNS — ответ DNS-сервера зависит от адреса клиента

Безопасность:
* DNSSEC — технология цифровой подписи записей DNS для подтверждения их подлинности
* DNS over TLS — технология передачи DNS-запроса и ответа поверх шифрованного TLS-соединения, для безопасной передачи данных.

Часто DNS используется для ограничения доступа к веб-сайтам. Примеры:
* Роскомнадзор
* Яндекс.DNS

Dynamic DNS — технология динамического изменения адреса
