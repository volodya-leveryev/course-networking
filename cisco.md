# Общие вопросы
Включение привилегированного режима:
```
Router$enable
```

Включение режима конфигурирования:
```
Router#configure terminal
```

Просмотр команд текущей настройки:
```
Router#show running-config
```

Сохранение текущей настройки:
```
Router#copy running-config startup-config
```

Перезагрузка:
```
Router#reload
```

Выполнение команд привилегированного режима в режиме конфигурирования:
```
Router(config)#do show running-config
```

Отправка эхо-запросов ICMP (пингов):
```
Router$ping 192.168.1.1
```

Трассировка маршрута пересылки пакетов:
```
Router$traceroute 192.168.1.1
```

Аварийное прерывание команды:
```
Ctrl+Shift+6
```

Отключение настроенной команды (на примере команды ip router ospf):
```
Router(config)#no ip router ospf
```

Установка имени хоста:
```
Router(config)#hostname new-router
```

# Настройка сетевого интерфейса

Выбор сетевого интерфейса:
```
Router(config)#interface FastEthernet 0/0
```

Установка статического IP-адреса:
```
Router(config-if)#ip address 192.168.1.5 255.255.255.0
```

Установка IP-адреса с помощью DHCP:
```
Router(config-if)#ip address dhcp
```

Отключение сетевого интерфейса:
```
Router(config-if)#shutdown
```

Включение сетевого интерфейса:
```
Router(config-if)#no shutdown
```

Просмотр состояния сетевых интерфейсов:
```
Router#show ip interface brief
```

# Настройка трансляции адресов NAT

Выбор сетевого интерфейса:
```
Router(config)#interface FastEthernet 0/0
```

Объявление интерфейса внутренним
```
Router(config-f)#ip nat inside
```

Выбор сетевого интерфейса:
```
Router(config)#interface FastEthernet 0/1
```

Объявление интерфейса внешним
```
Router(config-if)#ip nat outside
```

Создание списка доступа
```
Router(config)#access-list 1 permit any
```

Включение механизма NAT
```
Router(config-acl)#ip nat inside source list 1 interface FastEthernet 0/1
```

# Настройки динамической маршрутизации OSPF

Включение маршрутизации OSPF:
```
Router(config)#router ospf 1
```

Добавление сетей к зоне OSPF:
```
Router(config)#network 192.168.0.0 0.0.0.255 area 0
Router(config)#network 192.168.2.0 0.0.0.255 area 0
```

Просмотр таблицы маршрутизации:
```
Router#show ip route
```

# Настройка DHCP-сервера

Включение DHCP-сервера:
```
Router(config)#service dhcp
```

Создание пула адресов с названием guest:
```
Router(config)#ip dhcp pool guest
```

Добавление IP-подсети к пулу:
```
Router(config-pool)#network 192.168.20.0 255.255.255.0
```

Установка шлюза по-умолчанию в пуле (опционально):
```
Router(config-pool)#default-router 192.168.20.1
```

Установка адреса DNS-сервера (опционально):
```
Router(config-pool)#dns-server 192.168.20.101
```

Установка имени домена (опционально):
```
Router(config-pool)#domain-name example.com
```

Установка имени хоста (опционально):
```
Router(config-pool)#client-name glh-pc-2325
```

Установка адреса WINS-сервера (опционально):
```
Router(config-pool)#netbios-name-server 192.168.20.101
```

Установка дополнительных параметров (опционально):
```
Router(config-pool)#option 66 ip 10.44.2.1 10.44.2.2
```

Исключение адреса из пула (опционально):
```
Router(config)#ip dhcp excluded-address 192.168.20.1
```

Если необходимо создать резервирование IP-адреса клиента по его MAC-адресу, то для каждого такого клиента необходимо создать выделенный пул. Такой пул в отличие от обычного пула не содержит IP-подсети. Вместо этого необходимо указать его IP-адрес и MAC-адрес. Перед MAC-адресом необходимо указать префикс 01 (для сетей Microsoft) или 00 (для сетей Linux или Unix) Пример:
```
Router(config)#ip dhcp pool glh-pc-5443
Router(config-pool)#host 10.84.130.10 255.255.255.0
Router(config-pool)#client-identifier 0100.2264.15c4.96
Router(config-pool)#client-name glh-pc-5443
Router(config-pool)#domain-name glh.sm.aval
```

Просмотр выданных IP-адресов:
```
Router#show ip dhcp binding
```

Статистика DHCP-сервера:
```
Router#show ip dhcp server statistics
```

# Статическая маршрутизация

Добавление статического маршрута:
```
Router#ip route 192.168.5.0 255.255.255.0 192.168.1.1
```

Просмотр таблицы маршрутизации:
```
Router#show ip route
```

# Настройка VLAN на маршрутизаторе

Выбор сетевого интерфейса:
```
Router(config)#interface FastEthernet 0/0.1
```

Настройка подинтерфейса для работы с виртуальной сетью:
```
Router(config-subif)#encapsulation dot1Q 1234
```

```
Router#show ip 
```

# Настройка VLAN на коммутаторе

Выбор сетевого интерфейса:
```
Router(config)#interface FastEthernet 0/0
```

Выбор сетевого интерфейса:
```
Router(config-if)#interface FastEthernet 0/0
```

# Источники
1. http://xgu.ru/
2. https://linkmeup.ru/

**TODO: Туннели GRE**
